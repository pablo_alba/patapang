extends TextureRect


# Called when the node enters the scene tree for the first time.
func _ready():
	if Globals.music:
		$Music.play()			
	update_store()
		
func update_store():
	$Money/Label.text = str(Globals.coins)
	if Globals.adventure_girl_unblocked:
		$HBoxContainer/Player2/Player.character = "adventure_girl"
		$HBoxContainer/Player2/Button/Label.text = "Elegir"
		$HBoxContainer/Player2/Price.hide()		
	else:
		$HBoxContainer/Player2/Player.character = "adventure_girl_shadow"
		$HBoxContainer/Player2/Button/Label.text = "Comprar"
		$HBoxContainer/Player2/Price.show()
	
	if Globals.ninja_guy_unblocked:
		$HBoxContainer/Player3/Player.character = "ninja_guy"
		$HBoxContainer/Player3/Button/Label.text = "Elegir"
		$HBoxContainer/Player3/Price.hide()
	else:
		$HBoxContainer/Player3/Player.character = "ninja_guy_shadow"
		$HBoxContainer/Player3/Button/Label.text = "Comprar"
		$HBoxContainer/Player3/Price.show()
	
	if Globals.ninja_girl_unblocked:
		$HBoxContainer/Player4/Player.character = "ninja_girl"
		$HBoxContainer/Player4/Button/Label.text = "Elegir"
		$HBoxContainer/Player4/Price.hide()
	else:
		$HBoxContainer/Player4/Player.character = "ninja_girl_shadow"
		$HBoxContainer/Player4/Button/Label.text = "Comprar"
		$HBoxContainer/Player4/Price.show()
		
	if Globals.robot_unblocked:
		$HBoxContainer/Player5/Player.character = "robot"
		$HBoxContainer/Player5/Button/Label.text = "Elegir"
		$HBoxContainer/Player5/Price.hide()
	else:
		$HBoxContainer/Player5/Player.character = "robot_shadow"
		$HBoxContainer/Player5/Button/Label.text = "Comprar"
		$HBoxContainer/Player5/Price.show()
		
	$HBoxContainer/Player2/Player.idle()
	$HBoxContainer/Player3/Player.idle()
	$HBoxContainer/Player4/Player.idle()
	$HBoxContainer/Player5/Player.idle()


func _on_Close_gui_input(event):
	if event is InputEventMouseButton and event.pressed and event.button_index == 1:
		Globals.save_game()
		get_tree().change_scene("res://scenes/MainMenu.tscn")


func hide_all_selected():
	$HBoxContainer/Player1/Selected.hide()
	$HBoxContainer/Player2/Selected.hide()
	$HBoxContainer/Player3/Selected.hide()
	$HBoxContainer/Player4/Selected.hide()
	$HBoxContainer/Player5/Selected.hide()
	$AudioSelect.play()

func _on_Player1_Button_gui_input(event):
	if event is InputEventMouseButton and event.pressed and event.button_index == 1:
		Globals.character = "adventure_guy"
		hide_all_selected()
		$HBoxContainer/Player1/Selected.show()


func _on_Player2_Button_gui_input(event):
	if event is InputEventMouseButton and event.pressed and event.button_index == 1:
		if !Globals.adventure_girl_unblocked and Globals.coins >= 5:
			Globals.adventure_girl_unblocked = true
			pay()
			
		if Globals.adventure_girl_unblocked:
			Globals.character = "adventure_girl"
			hide_all_selected()
			$HBoxContainer/Player2/Selected.show()
			
			
func pay():
	if Globals.fx:
		$AudioBuy.play()
	Globals.coins -= 5
	$Money/Label.text = str(Globals.coins)
	update_store()
	Globals.save_game()
			


func _on_Player3_Button_gui_input(event):
	if event is InputEventMouseButton and event.pressed and event.button_index == 1:
		if !Globals.ninja_guy_unblocked and Globals.coins >= 5:
			Globals.ninja_guy_unblocked = true
			pay()
			
		if Globals.ninja_guy_unblocked:
			Globals.character = "ninja_guy"
			hide_all_selected()
			$HBoxContainer/Player3/Selected.show()


func _on_Player4_Button_gui_input(event):
	if event is InputEventMouseButton and event.pressed and event.button_index == 1:
		if !Globals.ninja_girl_unblocked and Globals.coins >= 5:
			Globals.ninja_girl_unblocked = true
			pay()
			
		if Globals.ninja_girl_unblocked:
			Globals.character = "ninja_girl"
			hide_all_selected()
			$HBoxContainer/Player4/Selected.show()


func _on_Player5_Button_gui_input(event):
	if event is InputEventMouseButton and event.pressed and event.button_index == 1:
		if !Globals.robot_unblocked and Globals.coins >= 5:			
			Globals.robot_unblocked = true
			pay()
			
		if Globals.robot_unblocked:
			Globals.character = "robot"
			hide_all_selected()
			$HBoxContainer/Player5/Selected.show()
