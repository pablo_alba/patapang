extends Area2D

var speed = 150
var motion = Vector2(0, speed)
var powerup
signal powerup_pickup(powerup)



# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func init(pw, pos, texture):
	powerup = pw
	$TextureRect.texture = texture
	position.x = pos.x
	position.y = pos.y
	

func _physics_process(delta):
	if ! Globals.paused:
		position.y += speed * delta
			


func _on_Powerup_body_entered(body):
	if "Floor" == body.name:
		speed = 0
	elif ("Player" == body.name):
		emit_signal("powerup_pickup", self)
	
