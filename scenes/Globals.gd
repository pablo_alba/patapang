extends Node

const POWERUP_WEAPON_BASIC = 0
const POWERUP_WEAPON_DOUBLE = 1
const POWERUP_WEAPON_HOOK = 2
const POWERUP_WEAPON_PISTOL = 3
const POWERUP_COIN = 4
const POWERUP_TIME = 5
const POWERUP_SHIELD = 6
const POWERUP_DYNAMITE = 7


var paused = false
var controls_move_left = false
var controls_move_right = false
var controls_fire = false
var timeStop = false

var backgrounds = [
	preload("res://resources/images/bg/0.png"),
	preload("res://resources/images/bg/1.png"),
	preload("res://resources/images/bg/2.png"),
	preload("res://resources/images/bg/3.png"),
	preload("res://resources/images/bg/4.png"),
	preload("res://resources/images/bg/5.png")	
];

#To save
var character = "adventure_guy"
var music = true
var fx = true
var adventure_girl_unblocked = false
var ninja_guy_unblocked = false
var ninja_girl_unblocked = false
var robot_unblocked = false
var max_level = 0
var current_level = 0
var coins = 0

func save_game():
	var save_game = File.new()
	if save_game.open(str("user://patapang.save"), File.WRITE) == OK: # If the opening of the save file returns OK	
		var settings = {
			"character": character,
			"music": music,
			"fx": fx,
			"adventure_girl_unblocked": adventure_girl_unblocked,
			"ninja_guy_unblocked": ninja_guy_unblocked,
			"ninja_girl_unblocked": ninja_girl_unblocked,
			"robot_unblocked": robot_unblocked,
			"max_level": max_level,
			"current_level": current_level,
			"coins": coins
		}
			
		
		save_game.store_var(settings) # then we store the contents of the var save inside it
		save_game.close() # and we gracefully close the file :)

func load_game():
	var save_game = File.new() # We initialize the File class
	var loaded = false
	if save_game.open(str("user://patapang.save"), File.READ_WRITE) == OK: # If the opening of the save file returns OK
		var temp_d # we create a temporary var to hold the contents of the save file
		temp_d = save_game.get_var() # we get the contents of the save file and store it on TEMP_D
		save_game.close()
		
		character = temp_d["character"]
		music = temp_d["music"]
		fx = temp_d["fx"]
		adventure_girl_unblocked = temp_d["adventure_girl_unblocked"]
		ninja_guy_unblocked = temp_d["ninja_guy_unblocked"]
		ninja_girl_unblocked = temp_d["ninja_girl_unblocked"]
		robot_unblocked = temp_d["robot_unblocked"]
		max_level = temp_d["max_level"]
		current_level = temp_d["current_level"]
		coins = temp_d["coins"]
	if not loaded:
		print("creating new savegame")
		save_game()		
