extends TextureRect
var current_stage

func _ready():
	if Globals.music:
		$Music.play()
	current_stage = floor(Globals.max_level / 9)
	show_stage()
	for stage in range (10):
		for n in range(9):
			var level = load("res://scenes/LevelNumber.tscn").instance()
			level.init(stage*9 + n, str(n+1))
			level.connect("select_level", self, "select_level")
			get_node("Stage"+str(stage)+"/GridContainer").add_child(level)
		
func show_stage():
	for stage in range (10):
		get_node("Stage"+str(stage)).hide()
	get_node("Stage"+str(current_stage)).show()
		
	if current_stage == 0:
		$Left.hide()
	else:
		$Left.show()
		
	if current_stage == 5:
		$Right.hide()
	else:
		$Right.show()
			
	


func _on_Home_gui_input(event):
	if event is InputEventMouseButton and event.pressed and event.button_index == 1:
		get_tree().change_scene("res://scenes/MainMenu.tscn")

func select_level(num_level):
	if Globals.fx:
		$AudioSelect.play()
	Globals.current_level = num_level	
	var error = get_tree().change_scene("res://scenes/Play.tscn")	
	if error:
		print("ERROR: could not change scene, code: ", error)

func _on_Left_gui_input(event):
	if event is InputEventMouseButton and event.pressed and event.button_index == 1:
		current_stage -=1		
		show_stage()
		
func _on_Right_gui_input(event):
	if event is InputEventMouseButton and event.pressed and event.button_index == 1:
		current_stage +=1		
		show_stage()
		
