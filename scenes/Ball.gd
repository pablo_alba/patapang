extends KinematicBody2D

const GRAVITY = 10
const SIZE_FULL = 100
const SIZE_GIGANT = 80
const SIZE_BIG = 60
const SIZE_MEDIUM = 40
const SIZE_SMALL = 20

var size = SIZE_BIG
var color
var motion = Vector2(3,0)
signal ball_break(ball, arrow)
signal player_hit(ball)
const is_ball = true


func _ready():
	pass # Replace with function body.
	
func init(ball_size, pos, move_right, ball_color, texture):
	if "full" == ball_size:
		size = SIZE_FULL		
	elif "gigant" == ball_size:
		size = SIZE_GIGANT
	elif "big" == ball_size:
		size = SIZE_BIG
	elif "medium" == ball_size:
		size = SIZE_MEDIUM
	else:
		size = SIZE_SMALL
		
	scale.x = size * 0.01
	scale.y = size * 0.01
	
	if ! move_right:
		motion.x = - motion.x
		
	position.x = pos.x
	position.y = pos.y
	color = ball_color
	$TextureRect.texture = texture
	
	
func bounce():
	motion.y = - 7
	
func hit_shield():
	motion.y = - motion.y
	motion.x = - motion.x
	
func _physics_process(delta):
	if ! Globals.paused && ! Globals.timeStop:
		motion.y += delta * GRAVITY
		var collision=move_and_collide(motion)
		if (collision != null):
			if ("Floor" == collision.collider.name):
				motion.y = - get_bounce_vel()
			elif ("Wall1" == collision.collider.name or "Wall2" == collision.collider.name):
				motion.x = - motion.x
			elif ("Arrow" == collision.collider.name):			
				emit_signal("ball_break", self, collision.collider)
			elif ("Player" == collision.collider.name):
				emit_signal("player_hit", self)

func get_bounce_vel():
	if size == SIZE_FULL:
		return 16
	elif size == SIZE_GIGANT:
		return 15
	elif size == SIZE_BIG:
		return 14
	elif size == SIZE_MEDIUM:
		return 13
	elif size == SIZE_SMALL:
		return 12
	
