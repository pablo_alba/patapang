extends Node2D

const FLOOR = 715

var balls = []
var arrows = []
var powerups = []
var stones = []
var spikes = []
var player
var weapon
var max_shots
var countdown
var level_powerups = []

var powerup_weapon_basic_texture = preload("res://resources/images/arrows/weapon_simple.png")
var powerup_weapon_double_texture = preload("res://resources/images/arrows/weapon_double.png")
var powerup_weapon_hook_texture = preload("res://resources/images/arrows/weapon_hook.png")
var powerup_weapon_pistol_texture = preload("res://resources/images/arrows/pistol.png")
var powerup_coin_texture = preload("res://resources/images/arrows/coin.png")
var powerup_clock_texture = preload("res://resources/images/arrows/clock.png")
var powerup_shield_texture = preload("res://resources/images/arrows/shield.png")
var powerup_dynamite_texture = preload("res://resources/images/arrows/dynamite.png")
var powerups_textures = [powerup_weapon_basic_texture, powerup_weapon_double_texture, 
	powerup_weapon_hook_texture, powerup_weapon_pistol_texture, powerup_coin_texture,
	powerup_clock_texture, powerup_shield_texture, powerup_dynamite_texture
]
var ball_blue_texture = preload("res://resources/images/bubble_blue.png")
var ball_green_texture = preload("res://resources/images/bubble_green.png")
var ball_red_texture = preload("res://resources/images/bubble_red.png")
var ball_yellow_texture = preload("res://resources/images/bubble_yellow.png")

var weapon_flash_times = 0
var current_hook
var shield
var coins



# Called when the node enters the scene tree for the first time.
func _ready():
	get_node("Player").character = Globals.character
	player = get_node("Player")
	player.connect("fire", self, "player_fire")
	$TimerWeapon.connect("timeout", self, "weapon_time_end")
	start_level()
	
func _process(delta):
	# Bug. Sometimes player collides with the arrows and "fly"
	player.position.y = FLOOR
	player.can_fire = arrows.size() < max_shots
	
	
func cleanup():
	for ball in balls:
		remove_child(ball)
	balls.clear()
	
	for arrow in arrows:
		remove_child(arrow)
	arrows.clear()
	
	for pw in powerups:
		remove_child(pw)
	powerups.clear()
	
	for stone in stones:
		remove_child(stone)
	stones.clear()
	
	for spike in spikes:
		remove_child(spike)
	spikes.clear()
	
	level_powerups.clear()
	
	
	shield = false
	player.shield(false)
	current_hook = null
	coins = 0
	Globals.timeStop = false
	
	$Message.hide()
	
func start_level():
	cleanup()
	Globals.paused = true
	var stage = floor(Globals.current_level / 9)
	$Background.texture = Globals.backgrounds[stage]
	
	if Globals.music:
		$Music.seek(0)
		$Music.play()
		
	$Controls/Coins/Label.text = str(coins)
		
	var level = $Levels.LEVELS[Globals.current_level]
	
	select_weapon(Globals.POWERUP_WEAPON_BASIC)
	player.idle()
	player.can_fire = true
	player.position.x = level["player_pos"]
	player.position.y = 715
	level_powerups = level["powerups"].duplicate()
	for ball_info in level["balls"]:		
		create_ball(ball_info["size"], ball_info["pos"], ball_info["move_right"], ball_info["color"])
		
	for stone_info in level["stones"]:		
		create_stone(stone_info)
	
	for spike_info in level["spikes"]:		
		create_spike(spike_info)
		
	countdown = 3
	$Countdown/Label.text = str(countdown)
	$Countdown.hide()	
	if Globals.current_level < 9:
		show_tutorial()
	else:
		$Countdown.show()
		invoke_after("ready_to_start", 1)

func ready_to_start():
	$Timer.disconnect("timeout", self, "ready_to_start")
	countdown -= 1
	if countdown == 0:		
		$Countdown.hide()
		Globals.paused = false
	else:
		$Countdown/Label.text = str(countdown)
		invoke_after("ready_to_start", 1)


func create_stone(position_x):
	var stone = load("res://scenes/Stone.tscn").instance()
	stone.position.x = position_x
	stone.position.y = FLOOR - 65
	add_child(stone)
	stones.append(stone)
	return stone

func create_spike(position_x):
	var spike = load("res://scenes/Spike.tscn").instance()
	spike.position.x = position_x
	spike.position.y = FLOOR - 10
	spike.connect("player_die", self, "player_die")
	add_child(spike)
	spikes.append(spike)
	return spike

func create_ball(size, pos, move_right, color):
	var ball = load("res://scenes/Ball.tscn").instance()
	ball.name = "Ball"	
	ball.init(size, pos, move_right, color, get_ball_texture(color))
	ball.connect("ball_break", self, "ball_break")
	ball.connect("player_hit", self, "player_hit")
	add_child(ball)
	balls.append(ball)
	return ball
	
func get_ball_texture(color):
	var texture = ball_red_texture
	if color == "blue":
		texture = ball_blue_texture
	elif color == "green":
		texture = ball_green_texture
	elif color == "yellow":
		texture = ball_yellow_texture
	return texture
	
func create_powerup(powerup, position):
	var pw = load("res://scenes/Powerup.tscn").instance()
	pw.init(powerup, position, powerups_textures[powerup])
	pw.connect("powerup_pickup", self, "powerup_pickup")
	add_child(pw)
	powerups.append(pw)
	return pw
	
func powerup_pickup(pw):
	if Globals.fx:
		$AudioPowerup.play()
	remove_child(pw)
	powerups.erase(pw)	
	if pw.powerup == Globals.POWERUP_COIN:
		coins += 1
		$Controls/Coins/Label.text = str(coins)
	elif pw.powerup == Globals.POWERUP_SHIELD:
		shield = true
		player.shield(true)
	elif pw.powerup == Globals.POWERUP_TIME:
		Globals.timeStop = true
		$TimerTimeStop.connect("timeout", self, "time_start")
		$TimerTimeStop.set_wait_time(4)
		$TimerTimeStop.start()
	elif pw.powerup == Globals.POWERUP_DYNAMITE:		
		var bigger_balls = true
		while bigger_balls:
			bigger_balls = false
			for ball in balls:
				if (ball.size != ball.SIZE_SMALL):
					bigger_balls = true
					ball_break(ball, null)
	else:
		select_weapon(pw.powerup)
	
func time_start():
	$TimerTimeStop.disconnect("timeout", self, "time_start")
	Globals.timeStop = false
	
func select_weapon(new_weapon):
	$TimerWeapon.stop()
	weapon_flash_times = 0
	weapon = new_weapon
	$Controls/Weapon.texture = powerups_textures[weapon]
	max_shots = 1
	if weapon == Globals.POWERUP_WEAPON_DOUBLE:
		max_shots = 2
	if weapon == Globals.POWERUP_WEAPON_PISTOL:
		max_shots = 10
		
	if weapon != Globals.POWERUP_WEAPON_BASIC:
		$TimerWeapon.set_wait_time(5)
		$TimerWeapon.start()

func player_fire():
	Globals.controls_fire = false
	var arrow
	if weapon == Globals.POWERUP_WEAPON_PISTOL:
		arrow = load("res://scenes/Bullet.tscn").instance()
	else:
		arrow = load("res://scenes/Arrow.tscn").instance()
	arrow.position.x = player.position.x
	arrow.position.y = player.position.y - 100
	arrow.type = weapon
	arrow.connect("arrow_ceil", self, "arrow_ceil")
	arrow.connect("ball_break", self, "ball_break")
	add_child(arrow)
	arrows.append(arrow)
	player.can_fire = arrows.size() < max_shots
	
func ball_break(ball, arrow):
	if Globals.fx:		
		$AudioPop.play()
	if arrow != null:
		remove_arrow(arrow)
	remove_ball(ball)
	player.can_fire = arrows.size() < max_shots
	
	var pw = level_powerups.pop_front()
	if pw != null:
		if pw != Globals.POWERUP_COIN or Globals.current_level == Globals.max_level: #Only give coins on max level
			create_powerup(pw, ball.position)
	
	if ball.size == ball.SIZE_SMALL:
		if balls.size() == 0:
			show_win_message()		
	else:
		split_ball(ball)

	
func split_ball(ball):
	var size
	if ball.size == ball.SIZE_MEDIUM:
		size = "small"
	elif ball.size == ball.SIZE_BIG:
		size = "medium"
	elif ball.size == ball.SIZE_GIGANT:
		size = "big"
	elif ball.size == ball.SIZE_FULL:
		size = "gigant"
		
	var new_ball = create_ball(size, Vector2(ball.position.x-10, ball.position.y), true, ball.color)
	new_ball.bounce()
	
	new_ball = create_ball(size, Vector2(ball.position.x+10, ball.position.y), false, ball.color)
	new_ball.bounce()	
	
	
	
	
func remove_arrow(arrow):
	arrow.disconnect("arrow_ceil", self, "arrow_ceil")
	arrow.disconnect("ball_break", self, "ball_break")
	remove_child(arrow)
	arrows.erase(arrow)

func remove_ball(ball):
	ball.disconnect("ball_break", self, "ball_break")
	ball.disconnect("player_hit", self, "player_hit")
	balls.erase(ball)
	remove_child(ball)	

func arrow_ceil(arrow):
	if arrow.type == Globals.POWERUP_WEAPON_HOOK:
		if current_hook == null:
			current_hook = arrow
			$TimerHook.connect("timeout", self, "unhook")
			$TimerHook.set_wait_time(1.5)
			$TimerHook.start()
	else:
		remove_arrow(arrow)
		player.can_fire = arrows.size() < max_shots
	
func unhook():
	$TimerHook.disconnect("timeout", self, "unhook")
	$TimerHook.stop()
	remove_arrow(current_hook)
	current_hook = null
	player.can_fire = arrows.size() < max_shots
	
func player_hit(ball):
	if shield:
		shield = false
		player.shield(false)		
		ball.hit_shield()
	else:
		player_die()
		
func player_die():
	if Globals.fx:
		$AudioDie.play()
	
	Globals.paused = true	
	player.die()
	invoke_after("show_failure_message", 1)
	
func invoke_after(function, seconds):	
	$Timer.connect("timeout", self, function)
	$Timer.set_wait_time(seconds)
	$Timer.start()
	
func show_failure_message():
	$Timer.disconnect("timeout", self, "show_failure_message")
	$Message/Next.hide()
	$Message/Retry.show()
	$Message/Label.text = "Fallaste"
	$Message.show()

func show_win_message():
	Globals.paused = true
	player.idle()
	if Globals.max_level == Globals.current_level:
		Globals.max_level += 1
		Globals.coins += coins
	if Globals.current_level < 90:
		$Message/Next.show()
	else:
		$Message/Next.hide()
	$Message/Retry.hide()
	$Message/Label.text = "¡BIEN HECHO!"
	$Message.show()
	Globals.save_game()


func _on_Home_gui_input(event):
	if event is InputEventMouseButton and event.pressed and event.button_index == 1:
		get_tree().change_scene("res://scenes/MainMenu.tscn")


func _on_Levels_gui_input(event):
	if event is InputEventMouseButton and event.pressed and event.button_index == 1:
		get_tree().change_scene("res://scenes/LevelSelect.tscn")


func _on_Next_gui_input(event):
	if event is InputEventMouseButton and event.pressed and event.button_index == 1:
		Globals.current_level += 1
		start_level()


func _on_Retry_gui_input(event):
	if event is InputEventMouseButton and event.pressed and event.button_index == 1:
		start_level()


func _on_Tutorial_Next_gui_input(event):
	if event is InputEventMouseButton and event.pressed and event.button_index == 1:
		get_node("Tutorial"+str(Globals.current_level)).hide()
		$Countdown.show()
		invoke_after("ready_to_start", 1)

func show_tutorial():
	get_node("Tutorial"+str(Globals.current_level)).show()


func _on_Left_gui_input(event):
	if event is InputEventScreenTouch:
		if event.pressed:
			Globals.controls_move_left = true
			Globals.controls_move_right = false
		else:
			Globals.controls_move_left = false

func _on_Right_gui_input(event):
	if event is InputEventScreenTouch:
		if event.pressed:
			Globals.controls_move_right = true
			Globals.controls_move_left = false
		else:
			Globals.controls_move_right = false


func _on_Fire_gui_input(event):
	if event is InputEventScreenTouch:
		if event.pressed:
			Globals.controls_fire = true
		else:
			Globals.controls_fire = false

func weapon_time_end():
	if weapon_flash_times < 12:
		if $Controls/Weapon.texture == null:
			$Controls/Weapon.texture = powerups_textures[weapon]
		else:
			$Controls/Weapon.texture = null		
		weapon_flash_times += 1
		$TimerWeapon.set_wait_time(0.25)
		$TimerWeapon.start()		
	else:
		select_weapon(Globals.POWERUP_WEAPON_BASIC)
		
