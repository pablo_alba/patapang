extends Area2D

signal player_die

func _ready():
	pass # Replace with function body.


func _on_Spike_body_entered(body):
	if (!Globals.paused) and ("Player" == body.name):
		emit_signal("player_die")
