extends TextureRect

func _ready():
	Globals.load_game()
	get_node("Player").character = Globals.character
	get_node("Player").idle()	
	show_music()
	show_fx()
	


func _on_Store_gui_input(event):
	if event is InputEventMouseButton and event.pressed and event.button_index == 1:
		get_tree().change_scene("res://scenes/Store.tscn")


func _on_Music_gui_input(event):
	if event is InputEventMouseButton and event.pressed and event.button_index == 1:
		Globals.music = false
		show_music()
		Globals.save_game()
		


func _on_MusicOff_gui_input(event):
	if event is InputEventMouseButton and event.pressed and event.button_index == 1:
		Globals.music = true
		show_music()
		Globals.save_game()


func _on_Fx_gui_input(event):
	if event is InputEventMouseButton and event.pressed and event.button_index == 1:
		Globals.fx = false
		show_fx()
		Globals.save_game()
		


func _on_FxOff_gui_input(event):
	if event is InputEventMouseButton and event.pressed and event.button_index == 1:
		Globals.fx = true
		show_fx()
		Globals.save_game()

func show_music():
	if Globals.music:
		get_node("Music").show()
		get_node("MusicOff").hide()
		get_node("AudioMusic").play()
	else:
		get_node("Music").hide()
		get_node("MusicOff").show()
		get_node("AudioMusic").stop()
		
func show_fx():
	if Globals.fx:
		get_node("Fx").show()
		get_node("FxOff").hide()
		get_node("AudioPop").play()
	else:
		get_node("Fx").hide()
		get_node("FxOff").show()
		get_node("AudioPop").stop()

func _on_Play_gui_input(event):
	if event is InputEventMouseButton and event.pressed and event.button_index == 1:
		get_tree().change_scene("res://scenes/LevelSelect.tscn")
		

	
