extends KinematicBody2D

var SPEED = -8
var  motion = Vector2(0,SPEED)
signal arrow_ceil(arrow)
signal ball_break(ball, arrow)
var type


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func _physics_process(delta):
	if ! Globals.paused:
		motion.y += SPEED * delta
		var collision = move_and_collide(motion)
		if (collision != null):
			print("COLISION "+collision.collider.name)
			if ("Ceil" == collision.collider.name):
				emit_signal("arrow_ceil", self)
			elif (collision.collider.get("is_ball")):
				emit_signal("ball_break", collision.collider, self)
