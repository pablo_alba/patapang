extends KinematicBody2D


var motion = Vector2()
const SPEED = 500
var is_firing = false
var can_fire = true
signal fire
var character = "adventure_guy"

func _ready():
	get_node("AnimatedSprite").animation = character + "_idle"

func _physics_process(delta):
	if ! Globals.paused:
		if is_firing :
			if get_node("AnimatedSprite").frame >=7:
				motion.x = 0
				get_node("AnimatedSprite").animation = character + "_idle"			
				is_firing = false
		else:
			if can_fire && (Input.is_action_pressed("ui_accept") || Globals.controls_fire):
				motion.x = 0
				get_node("AnimatedSprite").animation = character + "_fire"
				is_firing = true
				emit_signal("fire")
			elif Input.is_action_pressed("ui_right") || Globals.controls_move_right:
				motion.x = SPEED
				get_node("AnimatedSprite").flip_h = false
				get_node("AnimatedSprite").animation = character + "_walk"
			elif Input.is_action_pressed("ui_left") || Globals.controls_move_left:
				motion.x = -SPEED
				get_node("AnimatedSprite").flip_h = true
				get_node("AnimatedSprite").animation = character + "_walk"
			else:
				motion.x = 0
				get_node("AnimatedSprite").animation = character + "_idle"
			
		motion = move_and_slide(motion)
		
func die():
	get_node("AnimatedSprite").animation = character + "_die"
	get_node("AnimatedSprite").play()
	
func idle():
	get_node("AnimatedSprite").animation = character + "_idle"
	get_node("AnimatedSprite").play()
	
func shield(show):
	if show:
		$Shield.show()	
	else:
		$Shield.hide()
