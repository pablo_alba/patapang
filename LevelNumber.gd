extends TextureRect


signal select_level(level)
var num_level


func init(num, text):
	if num > Globals.max_level:
		$LevelLocked.show()
	else:
		num_level = num
		$Label.text = text
		$LevelLocked.hide()


func _on_LevelNumber_gui_input(event):
	if event is InputEventMouseButton and event.pressed and event.button_index == 1:
		if num_level != null:
			emit_signal("select_level", num_level)
