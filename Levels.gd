extends Node

const LEVELS = [
	{ # 0-1
		"player_pos": 930,
		"balls": [
			{
				"size": "medium",
				"pos": Vector2(1600, 200),
				"move_right":false,
				"color": "red"
			}
		],
		"powerups": [],
		"stones": [],
		"spikes": []
	},
	{ # 0-2
		"player_pos": 930,
		"balls": [
			{
				"size": "medium",
				"pos": Vector2(500, 200),
				"move_right":false,
				"color": "blue"
			},
			{
				"size": "medium",
				"pos": Vector2(1400, 200),
				"move_right":true,
				"color": "blue"
			}
		],
		"powerups": [Globals.POWERUP_WEAPON_DOUBLE, Globals.POWERUP_WEAPON_DOUBLE],
		"stones": [],
		"spikes": []
	},
	{ # 0-3
		"player_pos": 930,
		"balls": [
			{
				"size": "medium",
				"pos": Vector2(1300, 200),
				"move_right":false,
				"color": "yellow"
			},
			{
				"size": "medium",
				"pos": Vector2(1600, 200),
				"move_right":true,
				"color": "yellow"
			}
		],
		"powerups": [Globals.POWERUP_WEAPON_HOOK, null, Globals.POWERUP_WEAPON_HOOK],
		"stones": [],
		"spikes": []
	},
	{ # 0-4
		"player_pos": 930,
		"balls": [
			{
				"size": "big",
				"pos": Vector2(300, 200),
				"move_right":false,
				"color": "green"
			},
			{
				"size": "medium",
				"pos": Vector2(1600, 200),
				"move_right":true,
				"color": "green"
			}
		],
		"powerups": [Globals.POWERUP_WEAPON_PISTOL, null, null, Globals.POWERUP_WEAPON_PISTOL],
		"stones": [],
		"spikes": []
	},
	{ # 0-5
		"player_pos": 930,
		"balls": [
			{
				"size": "medium",
				"pos": Vector2(1500, 200),
				"move_right":false,
				"color": "red"
			},
			{
				"size": "medium",
				"pos": Vector2(300, 200),
				"move_right":true,
				"color": "red"
			}
		],
		"powerups": [Globals.POWERUP_COIN, null, Globals.POWERUP_COIN],
		"stones": [],
		"spikes": []
	},
	{ # 0-6
		"player_pos": 930,
		"balls": [
			{
				"size": "medium",
				"pos": Vector2(300, 200),
				"move_right":true,
				"color": "blue"
			},
			{
				"size": "medium",
				"pos": Vector2(1000, 200),
				"move_right":false,
				"color": "blue"
			},
			{
				"size": "medium",
				"pos": Vector2(1600, 200),
				"move_right":false,
				"color": "blue"
			}
		],
		"powerups": [Globals.POWERUP_SHIELD, null, Globals.POWERUP_COIN, null, Globals.POWERUP_SHIELD],
		"stones": [],
		"spikes": []
	},
	{ # 0-7
		"player_pos": 930,
		"balls": [
			{
				"size": "medium",
				"pos": Vector2(300, 200),
				"move_right":true,
				"color": "green"
			},
			{
				"size": "medium",
				"pos": Vector2(1000, 200),
				"move_right":false,
				"color": "green"
			},
			{
				"size": "medium",
				"pos": Vector2(1600, 200),
				"move_right":false,
				"color": "green"
			}
		],
		"powerups": [Globals.POWERUP_TIME, null, Globals.POWERUP_COIN, Globals.POWERUP_TIME],
		"stones": [],
		"spikes": []
	},
	{ # 0-8
		"player_pos": 930,
		"balls": [
			{
				"size": "gigant",
				"pos": Vector2(930, 200),
				"move_right":true,
				"color": "yellow"
			}
		],
		"powerups": [Globals.POWERUP_DYNAMITE, null, null, Globals.POWERUP_COIN],
		"stones": [],
		"spikes": []
	},
	{ # 0-9
		"player_pos": 930,
		"balls": [
			{
				"size": "gigant",
				"pos": Vector2(930, 200),
				"move_right":true,
				"color": "blue"
			}
		],
		"powerups": [null, null, Globals.POWERUP_COIN],
		"stones": [1480],
		"spikes": [340]
	},
	{ # 1-1
		"player_pos": 930,
		"balls": [
			{
				"size": "full",
				"pos": Vector2(300, 200),
				"move_right":true,
				"color": "red"
			}
		],
		"powerups": [Globals.POWERUP_WEAPON_DOUBLE, null, null, Globals.POWERUP_COIN, Globals.POWERUP_SHIELD, null, Globals.POWERUP_WEAPON_DOUBLE],
		"stones": [],
		"spikes": []
	},
	{ # 1-2
		"player_pos": 930,
		"balls": [
			{
				"size": "full",
				"pos": Vector2(300, 200),
				"move_right":true,
				"color": "green"
			}
		],
		"powerups": [Globals.POWERUP_WEAPON_PISTOL, Globals.POWERUP_TIME, null, null, Globals.POWERUP_TIME, Globals.POWERUP_COIN, Globals.POWERUP_WEAPON_HOOK, null, Globals.POWERUP_WEAPON_PISTOL],
		"stones": [],
		"spikes": []
	},
	{ # 1-3
		"player_pos": 930,
		"balls": [
			{
				"size": "full",
				"pos": Vector2(300, 200),
				"move_right":true,
				"color": "blue"
			},
			{
				"size": "medium",
				"pos": Vector2(1600, 200),
				"move_right":false,
				"color": "blue"
			}
		],
		"powerups": [Globals.POWERUP_WEAPON_HOOK, Globals.POWERUP_COIN, null, null, Globals.POWERUP_SHIELD, Globals.POWERUP_WEAPON_PISTOL, Globals.POWERUP_WEAPON_HOOK],
		"stones": [],
		"spikes": []
	},
	{ # 1-4
		"player_pos": 930,
		"balls": [
			{
				"size": "full",
				"pos": Vector2(300, 200),
				"move_right":true,
				"color": "green"
			}
		],
		"powerups": [Globals.POWERUP_SHIELD, null, Globals.POWERUP_WEAPON_HOOK, Globals.POWERUP_COIN, null, null, null, Globals.POWERUP_WEAPON_DOUBLE, Globals.POWERUP_WEAPON_HOOK],
		"stones": [1500],
		"spikes": []
	},
	{ # 1-5
		"player_pos": 930,
		"balls": [
			{
				"size": "big",
				"pos": Vector2(300, 200),
				"move_right":true,
				"color": "blue"
			},
			{
				"size": "big",
				"pos": Vector2(1600, 200),
				"move_right":false,
				"color": "blue"
			}
		],
		"powerups": [Globals.POWERUP_TIME, Globals.POWERUP_COIN, null, null, Globals.POWERUP_WEAPON_HOOK, null, null, null, null, Globals.POWERUP_WEAPON_DOUBLE],
		"stones": [250, 1500],
		"spikes": []
	},
	{ # 1-6
		"player_pos": 930,
		"balls": [
			{
				"size": "big",
				"pos": Vector2(300, 200),
				"move_right":false,
				"color": "red"
			},
			{
				"size": "full",
				"pos": Vector2(1600, 200),
				"move_right":true,
				"color": "red"
			}
		],
		"powerups": [Globals.POWERUP_WEAPON_DOUBLE, null, null, null, Globals.POWERUP_SHIELD, null, Globals.POWERUP_WEAPON_DOUBLE, null, null, null, Globals.POWERUP_COIN, null, null, Globals.POWERUP_WEAPON_HOOK, null, null, Globals.POWERUP_WEAPON_HOOK, null, null, Globals.POWERUP_WEAPON_PISTOL],
		"stones": [],
		"spikes": []
	},
	{ # 1-7
		"player_pos": 930,
		"balls": [
			{
				"size": "full",
				"pos": Vector2(300, 200),
				"move_right":false,
				"color": "yellow"
			},
			{
				"size": "big",
				"pos": Vector2(1600, 200),
				"move_right":true,
				"color": "yellow"
			}
		],
		"powerups": [null, null, Globals.POWERUP_TIME, Globals.POWERUP_WEAPON_DOUBLE, null, null, null, Globals.POWERUP_SHIELD, null, null, Globals.POWERUP_SHIELD, null, null, null, Globals.POWERUP_COIN, null, null, Globals.POWERUP_WEAPON_PISTOL, null, null, Globals.POWERUP_WEAPON_HOOK, null, null, Globals.POWERUP_SHIELD],
		"stones": [],
		"spikes": []
	},
	{ # 1-8
		"player_pos": 930,
		"balls": [
			{
				"size": "big",
				"pos": Vector2(300, 200),
				"move_right":false,
				"color": "blue"
			},
			{
				"size": "medium",
				"pos": Vector2(930, 200),
				"move_right":false,
				"color": "blue"
			},
			{
				"size": "big",
				"pos": Vector2(1600, 200),
				"move_right":true,
				"color": "blue"
			}
		],
		"powerups": [Globals.POWERUP_WEAPON_DOUBLE, null, null, null, null, Globals.POWERUP_WEAPON_DOUBLE, Globals.POWERUP_DYNAMITE, null, null, null, Globals.POWERUP_SHIELD, null, Globals.POWERUP_COIN, null, null, null, Globals.POWERUP_WEAPON_PISTOL, null, null, Globals.POWERUP_WEAPON_PISTOL, null, null, Globals.POWERUP_WEAPON_HOOK],
		"stones": [],
		"spikes": [350]
	},
	{ # 1-9
		"player_pos": 930,
		"balls": [
			{
				"size": "full",
				"pos": Vector2(300, 200),
				"move_right":true,
				"color": "red"
			}
		],
		"powerups": [Globals.POWERUP_DYNAMITE, null, null, null, null, Globals.POWERUP_WEAPON_DOUBLE, Globals.POWERUP_WEAPON_DOUBLE, null, null, null, Globals.POWERUP_COIN, null, null, null, null, null, null, Globals.POWERUP_WEAPON_PISTOL, null, null, Globals.POWERUP_WEAPON_PISTOL, null, null, Globals.POWERUP_WEAPON_HOOK],
		"stones": [],
		"spikes": []
	},
	{ # 2-1
		"player_pos": 930,
		"balls": [
			{
				"size": "full",
				"pos": Vector2(300, 200),
				"move_right":false,
				"color": "blue"
			},
			{
				"size": "small",
				"pos": Vector2(600, 200),
				"move_right":false,
				"color": "blue"
			},
			{
				"size": "small",
				"pos": Vector2(900, 200),
				"move_right":false,
				"color": "blue"
			}
		],
		"powerups": [Globals.POWERUP_WEAPON_HOOK, null, null, null, Globals.POWERUP_WEAPON_HOOK, Globals.POWERUP_SHIELD, null, null, null, Globals.POWERUP_TIME, Globals.POWERUP_COIN, null, null, null, null, Globals.POWERUP_WEAPON_DOUBLE],
		"stones": [],
		"spikes": []
	},
	{ # 2-2
		"player_pos": 930,
		"balls": [
			{
				"size": "full",
				"pos": Vector2(300, 200),
				"move_right":true,
				"color": "green"
			}
		],
		"powerups": [Globals.POWERUP_WEAPON_PISTOL, Globals.POWERUP_TIME, null, null, Globals.POWERUP_TIME, Globals.POWERUP_COIN, Globals.POWERUP_WEAPON_HOOK, null, Globals.POWERUP_WEAPON_PISTOL],
		"stones": [300, 1600],
		"spikes": []
	},
	{ # 2-3
		"player_pos": 930,
		"balls": [
			{
				"size": "big",
				"pos": Vector2(300, 200),
				"move_right":true,
				"color": "red"
			},
			{
				"size": "medium",
				"pos": Vector2(1600, 200),
				"move_right":false,
				"color": "red"
			}
		],
		"powerups": [Globals.POWERUP_WEAPON_HOOK, Globals.POWERUP_COIN, null, null, Globals.POWERUP_SHIELD, Globals.POWERUP_WEAPON_PISTOL, Globals.POWERUP_WEAPON_HOOK],
		"stones": [],
		"spikes": [500]
	},
	{ # 2-4
		"player_pos": 930,
		"balls": [
			{
				"size": "small",
				"pos": Vector2(100, 200),
				"move_right":true,
				"color": "yellow"
			},
			{
				"size": "small",
				"pos": Vector2(400, 200),
				"move_right":true,
				"color": "yellow"
			},
			{
				"size": "small",
				"pos": Vector2(700, 200),
				"move_right":true,
				"color": "yellow"
			},
			{
				"size": "small",
				"pos": Vector2(1000, 200),
				"move_right":true,
				"color": "yellow"
			},
			{
				"size": "small",
				"pos": Vector2(1300, 200),
				"move_right":true,
				"color": "yellow"
			},
			{
				"size": "small",
				"pos": Vector2(1600, 200),
				"move_right":true,
				"color": "yellow"
			}
		],
		"powerups": [Globals.POWERUP_WEAPON_DOUBLE, Globals.POWERUP_COIN, null, null, Globals.POWERUP_TIME, null, Globals.POWERUP_WEAPON_PISTOL, Globals.POWERUP_WEAPON_HOOK],
		"stones": [],
		"spikes": []
	},
	{ # 2-5
		"player_pos": 930,
		"balls": [
			{
				"size": "full",
				"pos": Vector2(100, 200),
				"move_right":true,
				"color": "blue"
			},
			{
				"size": "full",
				"pos": Vector2(1600, 200),
				"move_right":false,
				"color": "blue"
			}
		],
		"powerups": [null, null, null, Globals.POWERUP_WEAPON_DOUBLE, null, null, null, Globals.POWERUP_COIN, null, Globals.POWERUP_WEAPON_PISTOL, null, null, Globals.POWERUP_WEAPON_HOOK, null, Globals.POWERUP_TIME, null, null, null, Globals.POWERUP_WEAPON_DOUBLE],
		"stones": [],
		"spikes": []
	},
	{ # 2-6
		"player_pos": 930,
		"balls": [
			{
				"size": "small",
				"pos": Vector2(100, 200),
				"move_right":true,
				"color": "blue"
			},
			{
				"size": "small",
				"pos": Vector2(400, 200),
				"move_right":true,
				"color": "blue"
			},
			{
				"size": "medium",
				"pos": Vector2(700, 200),
				"move_right":true,
				"color": "blue"
			},
			{
				"size": "medium",
				"pos": Vector2(1000, 200),
				"move_right":true,
				"color": "blue"
			},
			{
				"size": "small",
				"pos": Vector2(1300, 200),
				"move_right":true,
				"color": "blue"
			},
			{
				"size": "small",
				"pos": Vector2(1600, 200),
				"move_right":true,
				"color": "blue"
			}
		],
		"powerups": [Globals.POWERUP_WEAPON_DOUBLE, Globals.POWERUP_COIN, null, null, Globals.POWERUP_TIME, null, Globals.POWERUP_WEAPON_PISTOL, Globals.POWERUP_WEAPON_HOOK],
		"stones": [],
		"spikes": []
	},
	{ # 2-7
		"player_pos": 930,
		"balls": [
			{
				"size": "full",
				"pos": Vector2(300, 200),
				"move_right":true,
				"color": "green"
			},
			{
				"size": "big",
				"pos": Vector2(1000, 200),
				"move_right":true,
				"color": "green"
			},
			{
				"size": "full",
				"pos": Vector2(1600, 200),
				"move_right":false,
				"color": "green"
			}
		],
		"powerups": [null, null, null, Globals.POWERUP_WEAPON_DOUBLE, null, null, null, Globals.POWERUP_WEAPON_HOOK, null, Globals.POWERUP_WEAPON_PISTOL, null, null, Globals.POWERUP_COIN, null, Globals.POWERUP_DYNAMITE, null, null, null, Globals.POWERUP_WEAPON_DOUBLE, null, null, Globals.POWERUP_WEAPON_HOOK, null, null, null, Globals.POWERUP_WEAPON_DOUBLE, null, null, null, Globals.POWERUP_WEAPON_HOOK, null, Globals.POWERUP_WEAPON_PISTOL],
		"stones": [],
		"spikes": []
	},
	{ #2-8
	"player_pos": 930,
	"balls": [
		{
			"size": "big",
			"pos": Vector2(300, 200),
			"move_right":true,
			"color": "yellow"
		},
		{
			"size": "full",
			"pos": Vector2(930, 200),
			"move_right":true,
			"color": "yellow"
		},
		{
			"size": "medium",
			"pos": Vector2(1600, 200),
			"move_right":false,
			"color": "yellow"
		}
	],
	"powerups": [null,Globals.POWERUP_COIN,Globals.POWERUP_WEAPON_PISTOL,null,null,Globals.POWERUP_DYNAMITE,Globals.POWERUP_WEAPON_DOUBLE,null,null,null,null,Globals.POWERUP_WEAPON_DOUBLE,null,Globals.POWERUP_SHIELD,null,],
	"stones": [],
	"spikes": []
},
{ #2-9
	"player_pos": 930,
	"balls": [
		{
			"size": "full",
			"pos": Vector2(300, 200),
			"move_right":true,
			"color": "blue"
		},
		{
			"size": "full",
			"pos": Vector2(1600, 200),
			"move_right":false,
			"color": "blue"
		}
	],
	"powerups": [null,Globals.POWERUP_COIN,null,Globals.POWERUP_SHIELD,Globals.POWERUP_WEAPON_PISTOL,Globals.POWERUP_DYNAMITE,Globals.POWERUP_DYNAMITE,null,null,null,null,null,null,null,null,null,null,Globals.POWERUP_WEAPON_HOOK,null,Globals.POWERUP_WEAPON_DOUBLE,null,null,null,null,Globals.POWERUP_WEAPON_DOUBLE,],
	"stones": [],
	"spikes": []
},
{ #3-0
	"player_pos": 930,
	"balls": [
		{
			"size": "medium",
			"pos": Vector2(300, 200),
			"move_right":false,
			"color": "yellow"
		},
		{
			"size": "full",
			"pos": Vector2(930, 200),
			"move_right":true,
			"color": "yellow"
		},
		{
			"size": "big",
			"pos": Vector2(1600, 200),
			"move_right":false,
			"color": "yellow"
		}
	],
	"powerups": [Globals.POWERUP_COIN,null,null,null,null,null,null,null,Globals.POWERUP_WEAPON_DOUBLE,Globals.POWERUP_DYNAMITE,null,null,null,Globals.POWERUP_TIME,Globals.POWERUP_WEAPON_DOUBLE,Globals.POWERUP_WEAPON_PISTOL,Globals.POWERUP_TIME,null,Globals.POWERUP_DYNAMITE,Globals.POWERUP_WEAPON_PISTOL,null,Globals.POWERUP_SHIELD,null,null,null,],
	"stones": [],
	"spikes": [1450]
},
{ #3-1
	"player_pos": 930,
	"balls": [
		{
			"size": "big",
			"pos": Vector2(300, 200),
			"move_right":true,
			"color": "yellow"
		},
		{
			"size": "full",
			"pos": Vector2(930, 200),
			"move_right":true,
			"color": "yellow"
		},
		{
			"size": "medium",
			"pos": Vector2(1600, 200),
			"move_right":false,
			"color": "yellow"
		}
	],
	"powerups": [null,null,null,null,null,null,null,Globals.POWERUP_COIN,Globals.POWERUP_WEAPON_PISTOL,Globals.POWERUP_WEAPON_DOUBLE,Globals.POWERUP_WEAPON_DOUBLE,null,null,Globals.POWERUP_DYNAMITE,Globals.POWERUP_WEAPON_HOOK,null,Globals.POWERUP_SHIELD,Globals.POWERUP_SHIELD,null,null,null,null,null,Globals.POWERUP_WEAPON_HOOK,Globals.POWERUP_DYNAMITE,],
	"stones": [350],
	"spikes": []
},
{ #3-2
	"player_pos": 930,
	"balls": [
		{
			"size": "small",
			"pos": Vector2(100, 200),
			"move_right":false,
			"color": "blue"
		},
		{
			"size": "medium",
			"pos": Vector2(400, 200),
			"move_right":false,
			"color": "blue"
		},
		{
			"size": "big",
			"pos": Vector2(930, 200),
			"move_right":true,
			"color": "blue"
		},
		{
			"size": "medium",
			"pos": Vector2(1200, 200),
			"move_right":true,
			"color": "blue"
		},
		{
			"size": "small",
			"pos": Vector2(1600, 200),
			"move_right":true,
			"color": "blue"
		}
	],
	"powerups": [null,null,Globals.POWERUP_TIME,null,null,null,null,Globals.POWERUP_COIN,null,Globals.POWERUP_DYNAMITE,null,null,null,null,Globals.POWERUP_WEAPON_HOOK,null,null,null,Globals.POWERUP_WEAPON_HOOK,null,Globals.POWERUP_SHIELD,null,null,Globals.POWERUP_DYNAMITE,null,],
	"stones": [],
	"spikes": []
},
{ #3-3
	"player_pos": 930,
	"balls": [
		{
			"size": "full",
			"pos": Vector2(300, 200),
			"move_right":true,
			"color": "green"
		},
		{
			"size": "full",
			"pos": Vector2(1600, 200),
			"move_right":false,
			"color": "green"
		}
	],
	"powerups": [null,Globals.POWERUP_WEAPON_HOOK,null,Globals.POWERUP_COIN,Globals.POWERUP_WEAPON_DOUBLE,Globals.POWERUP_WEAPON_PISTOL,Globals.POWERUP_WEAPON_DOUBLE,null,null,Globals.POWERUP_TIME,null,null,null,null,null,Globals.POWERUP_TIME,null,null,null,Globals.POWERUP_DYNAMITE,null,null,null,null,null,],
	"stones": [],
	"spikes": [350]
},
{ #3-4
	"player_pos": 930,
	"balls": [
		{
			"size": "big",
			"pos": Vector2(300, 200),
			"move_right":true,
			"color": "red"
		},
		{
			"size": "full",
			"pos": Vector2(930, 200),
			"move_right":true,
			"color": "red"
		},
		{
			"size": "medium",
			"pos": Vector2(1600, 200),
			"move_right":false,
			"color": "red"
		}
	],
	"powerups": [Globals.POWERUP_WEAPON_DOUBLE,Globals.POWERUP_DYNAMITE,Globals.POWERUP_COIN,Globals.POWERUP_WEAPON_HOOK,null,Globals.POWERUP_WEAPON_HOOK,Globals.POWERUP_WEAPON_HOOK,Globals.POWERUP_SHIELD,null,null,null,null,Globals.POWERUP_SHIELD,null,null,null,null,null,null,null,null,null,null,Globals.POWERUP_DYNAMITE,Globals.POWERUP_TIME,],
	"stones": [],
	"spikes": []
},
{ #3-5
	"player_pos": 930,
	"balls": [
		{
			"size": "medium",
			"pos": Vector2(300, 200),
			"move_right":false,
			"color": "blue"
		},
		{
			"size": "full",
			"pos": Vector2(930, 200),
			"move_right":true,
			"color": "blue"
		},
		{
			"size": "big",
			"pos": Vector2(1600, 200),
			"move_right":false,
			"color": "blue"
		}
	],
	"powerups": [null,Globals.POWERUP_WEAPON_HOOK,null,Globals.POWERUP_COIN,null,null,null,null,null,Globals.POWERUP_DYNAMITE,null,null,Globals.POWERUP_WEAPON_PISTOL,null,Globals.POWERUP_WEAPON_DOUBLE,null,Globals.POWERUP_WEAPON_HOOK,null,Globals.POWERUP_TIME,null,null,null,null,null,Globals.POWERUP_WEAPON_DOUBLE,],
	"stones": [],
	"spikes": []
},
{ #3-6
	"player_pos": 930,
	"balls": [
		{
			"size": "full",
			"pos": Vector2(300, 200),
			"move_right":true,
			"color": "yellow"
		},
		{
			"size": "full",
			"pos": Vector2(1600, 200),
			"move_right":false,
			"color": "yellow"
		}
	],
	"powerups": [Globals.POWERUP_WEAPON_PISTOL,null,Globals.POWERUP_COIN,null,null,null,Globals.POWERUP_WEAPON_HOOK,Globals.POWERUP_TIME,Globals.POWERUP_WEAPON_PISTOL,null,null,null,null,null,null,Globals.POWERUP_WEAPON_DOUBLE,null,null,null,null,Globals.POWERUP_WEAPON_HOOK,null,null,Globals.POWERUP_SHIELD,null,],
	"stones": [],
	"spikes": []
},
{ #3-7
	"player_pos": 930,
	"balls": [
		{
			"size": "full",
			"pos": Vector2(300, 200),
			"move_right":true,
			"color": "yellow"
		},
		{
			"size": "full",
			"pos": Vector2(1600, 200),
			"move_right":false,
			"color": "yellow"
		}
	],
	"powerups": [null,null,Globals.POWERUP_COIN,Globals.POWERUP_SHIELD,null,Globals.POWERUP_DYNAMITE,null,Globals.POWERUP_WEAPON_PISTOL,null,null,Globals.POWERUP_SHIELD,null,null,null,null,Globals.POWERUP_TIME,null,Globals.POWERUP_TIME,null,null,Globals.POWERUP_TIME,null,Globals.POWERUP_TIME,null,null,],
	"stones": [],
	"spikes": [350]
},
{ #3-8
	"player_pos": 930,
	"balls": [
		{
			"size": "medium",
			"pos": Vector2(300, 200),
			"move_right":false,
			"color": "green"
		},
		{
			"size": "full",
			"pos": Vector2(930, 200),
			"move_right":true,
			"color": "green"
		},
		{
			"size": "big",
			"pos": Vector2(1600, 200),
			"move_right":false,
			"color": "green"
		}
	],
	"powerups": [Globals.POWERUP_SHIELD,null,null,null,Globals.POWERUP_DYNAMITE,Globals.POWERUP_WEAPON_HOOK,Globals.POWERUP_SHIELD,Globals.POWERUP_COIN,null,null,Globals.POWERUP_TIME,null,null,Globals.POWERUP_WEAPON_HOOK,null,null,null,Globals.POWERUP_WEAPON_DOUBLE,null,Globals.POWERUP_WEAPON_HOOK,null,Globals.POWERUP_WEAPON_DOUBLE,null,Globals.POWERUP_WEAPON_PISTOL,null,],
	"stones": [350, 1450],
	"spikes": []
},
{ #4-0
	"player_pos": 930,
	"balls": [
		{
			"size": "big",
			"pos": Vector2(300, 200),
			"move_right":true,
			"color": "green"
		},
		{
			"size": "full",
			"pos": Vector2(930, 200),
			"move_right":true,
			"color": "green"
		},
		{
			"size": "medium",
			"pos": Vector2(1600, 200),
			"move_right":false,
			"color": "green"
		}
	],
	"powerups": [Globals.POWERUP_SHIELD,null,Globals.POWERUP_WEAPON_DOUBLE,null,Globals.POWERUP_WEAPON_HOOK,Globals.POWERUP_WEAPON_DOUBLE,null,Globals.POWERUP_COIN,null,Globals.POWERUP_TIME,Globals.POWERUP_SHIELD,Globals.POWERUP_WEAPON_PISTOL,null,null,null,null,Globals.POWERUP_DYNAMITE,null,null,null,null,null,Globals.POWERUP_SHIELD,Globals.POWERUP_DYNAMITE,null,],
	"stones": [],
	"spikes": []
},
{ #4-1
	"player_pos": 930,
	"balls": [
		{
			"size": "full",
			"pos": Vector2(300, 200),
			"move_right":true,
			"color": "green"
		},
		{
			"size": "full",
			"pos": Vector2(1600, 200),
			"move_right":false,
			"color": "green"
		}
	],
	"powerups": [null,null,null,null,null,null,null,null,Globals.POWERUP_WEAPON_HOOK,Globals.POWERUP_COIN,Globals.POWERUP_WEAPON_PISTOL,null,null,null,Globals.POWERUP_SHIELD,Globals.POWERUP_TIME,Globals.POWERUP_WEAPON_PISTOL,Globals.POWERUP_TIME,null,Globals.POWERUP_DYNAMITE,null,null,null,null,Globals.POWERUP_SHIELD,],
	"stones": [],
	"spikes": [1450]
},
{ #4-2
	"player_pos": 930,
	"balls": [
		{
			"size": "full",
			"pos": Vector2(300, 200),
			"move_right":true,
			"color": "yellow"
		},
		{
			"size": "full",
			"pos": Vector2(1600, 200),
			"move_right":false,
			"color": "yellow"
		}
	],
	"powerups": [Globals.POWERUP_WEAPON_DOUBLE,null,null,null,null,Globals.POWERUP_WEAPON_PISTOL,Globals.POWERUP_DYNAMITE,Globals.POWERUP_TIME,Globals.POWERUP_COIN,Globals.POWERUP_SHIELD,null,null,Globals.POWERUP_WEAPON_PISTOL,null,Globals.POWERUP_SHIELD,Globals.POWERUP_WEAPON_PISTOL,Globals.POWERUP_TIME,Globals.POWERUP_SHIELD,Globals.POWERUP_TIME,null,null,null,Globals.POWERUP_DYNAMITE,null,Globals.POWERUP_DYNAMITE,],
	"stones": [],
	"spikes": []
},
{ #4-3
	"player_pos": 930,
	"balls": [
		{
			"size": "small",
			"pos": Vector2(100, 200),
			"move_right":false,
			"color": "blue"
		},
		{
			"size": "medium",
			"pos": Vector2(400, 200),
			"move_right":false,
			"color": "blue"
		},
		{
			"size": "big",
			"pos": Vector2(930, 200),
			"move_right":true,
			"color": "blue"
		},
		{
			"size": "medium",
			"pos": Vector2(1200, 200),
			"move_right":true,
			"color": "blue"
		},
		{
			"size": "small",
			"pos": Vector2(1600, 200),
			"move_right":true,
			"color": "blue"
		}
	],
	"powerups": [null,null,null,Globals.POWERUP_TIME,Globals.POWERUP_TIME,Globals.POWERUP_COIN,null,Globals.POWERUP_WEAPON_PISTOL,null,null,null,Globals.POWERUP_TIME,null,Globals.POWERUP_WEAPON_HOOK,Globals.POWERUP_SHIELD,null,null,null,Globals.POWERUP_WEAPON_HOOK,null,null,Globals.POWERUP_SHIELD,Globals.POWERUP_WEAPON_DOUBLE,Globals.POWERUP_WEAPON_DOUBLE,null,],
	"stones": [],
	"spikes": []
},
{ #4-4
	"player_pos": 930,
	"balls": [
		{
			"size": "full",
			"pos": Vector2(300, 200),
			"move_right":true,
			"color": "green"
		},
		{
			"size": "full",
			"pos": Vector2(1600, 200),
			"move_right":false,
			"color": "green"
		}
	],
	"powerups": [Globals.POWERUP_WEAPON_PISTOL,Globals.POWERUP_DYNAMITE,null,Globals.POWERUP_WEAPON_DOUBLE,null,null,Globals.POWERUP_TIME,Globals.POWERUP_TIME,Globals.POWERUP_WEAPON_PISTOL,Globals.POWERUP_COIN,Globals.POWERUP_DYNAMITE,null,null,Globals.POWERUP_WEAPON_DOUBLE,null,null,null,null,Globals.POWERUP_TIME,Globals.POWERUP_WEAPON_PISTOL,Globals.POWERUP_SHIELD,null,null,null,null,],
	"stones": [],
	"spikes": []
},
{ #4-5
	"player_pos": 930,
	"balls": [
		{
			"size": "small",
			"pos": Vector2(100, 200),
			"move_right":false,
			"color": "blue"
		},
		{
			"size": "medium",
			"pos": Vector2(400, 200),
			"move_right":false,
			"color": "blue"
		},
		{
			"size": "big",
			"pos": Vector2(930, 200),
			"move_right":true,
			"color": "blue"
		},
		{
			"size": "medium",
			"pos": Vector2(1200, 200),
			"move_right":true,
			"color": "blue"
		},
		{
			"size": "small",
			"pos": Vector2(1600, 200),
			"move_right":true,
			"color": "blue"
		}
	],
	"powerups": [Globals.POWERUP_COIN,Globals.POWERUP_SHIELD,Globals.POWERUP_WEAPON_HOOK,Globals.POWERUP_WEAPON_HOOK,null,null,null,null,null,Globals.POWERUP_TIME,null,Globals.POWERUP_SHIELD,null,Globals.POWERUP_SHIELD,Globals.POWERUP_WEAPON_DOUBLE,Globals.POWERUP_WEAPON_DOUBLE,null,Globals.POWERUP_TIME,Globals.POWERUP_TIME,null,null,null,Globals.POWERUP_WEAPON_DOUBLE,null,null,],
	"stones": [1450],
	"spikes": []
},
{ #4-6
	"player_pos": 930,
	"balls": [
		{
			"size": "full",
			"pos": Vector2(300, 200),
			"move_right":true,
			"color": "yellow"
		},
		{
			"size": "full",
			"pos": Vector2(1600, 200),
			"move_right":false,
			"color": "yellow"
		}
	],
	"powerups": [Globals.POWERUP_SHIELD,Globals.POWERUP_WEAPON_HOOK,null,Globals.POWERUP_WEAPON_DOUBLE,null,Globals.POWERUP_WEAPON_DOUBLE,null,Globals.POWERUP_TIME,null,Globals.POWERUP_COIN,null,null,null,Globals.POWERUP_WEAPON_PISTOL,null,null,null,Globals.POWERUP_SHIELD,Globals.POWERUP_WEAPON_DOUBLE,null,null,null,Globals.POWERUP_SHIELD,null,null,],
	"stones": [],
	"spikes": []
},
{ #4-7
	"player_pos": 930,
	"balls": [
		{
			"size": "big",
			"pos": Vector2(300, 200),
			"move_right":true,
			"color": "green"
		},
		{
			"size": "full",
			"pos": Vector2(930, 200),
			"move_right":true,
			"color": "green"
		},
		{
			"size": "medium",
			"pos": Vector2(1600, 200),
			"move_right":false,
			"color": "green"
		}
	],
	"powerups": [null,null,null,null,Globals.POWERUP_WEAPON_PISTOL,Globals.POWERUP_COIN,Globals.POWERUP_WEAPON_HOOK,Globals.POWERUP_TIME,Globals.POWERUP_WEAPON_HOOK,null,null,Globals.POWERUP_WEAPON_HOOK,Globals.POWERUP_WEAPON_HOOK,null,null,null,null,Globals.POWERUP_DYNAMITE,null,null,null,null,Globals.POWERUP_WEAPON_PISTOL,null,null,],
	"stones": [],
	"spikes": []
},
{ #4-8
	"player_pos": 930,
	"balls": [
		{
			"size": "big",
			"pos": Vector2(300, 200),
			"move_right":true,
			"color": "yellow"
		},
		{
			"size": "full",
			"pos": Vector2(930, 200),
			"move_right":true,
			"color": "yellow"
		},
		{
			"size": "medium",
			"pos": Vector2(1600, 200),
			"move_right":false,
			"color": "yellow"
		}
	],
	"powerups": [Globals.POWERUP_TIME,Globals.POWERUP_WEAPON_PISTOL,null,Globals.POWERUP_WEAPON_PISTOL,null,Globals.POWERUP_WEAPON_HOOK,Globals.POWERUP_COIN,Globals.POWERUP_SHIELD,null,null,Globals.POWERUP_WEAPON_DOUBLE,Globals.POWERUP_WEAPON_DOUBLE,null,null,null,Globals.POWERUP_SHIELD,Globals.POWERUP_WEAPON_DOUBLE,null,null,Globals.POWERUP_DYNAMITE,null,Globals.POWERUP_TIME,null,Globals.POWERUP_SHIELD,null,],
	"stones": [350, 1450],
	"spikes": []
},
{ #5-0
	"player_pos": 930,
	"balls": [
		{
			"size": "small",
			"pos": Vector2(100, 200),
			"move_right":false,
			"color": "green"
		},
		{
			"size": "medium",
			"pos": Vector2(400, 200),
			"move_right":false,
			"color": "green"
		},
		{
			"size": "big",
			"pos": Vector2(930, 200),
			"move_right":true,
			"color": "green"
		},
		{
			"size": "medium",
			"pos": Vector2(1200, 200),
			"move_right":true,
			"color": "green"
		},
		{
			"size": "small",
			"pos": Vector2(1600, 200),
			"move_right":true,
			"color": "green"
		}
	],
	"powerups": [Globals.POWERUP_COIN,null,null,Globals.POWERUP_TIME,Globals.POWERUP_SHIELD,null,null,null,null,null,null,null,null,Globals.POWERUP_SHIELD,null,Globals.POWERUP_DYNAMITE,null,null,Globals.POWERUP_WEAPON_DOUBLE,Globals.POWERUP_WEAPON_PISTOL,Globals.POWERUP_WEAPON_DOUBLE,Globals.POWERUP_DYNAMITE,Globals.POWERUP_WEAPON_DOUBLE,null,null,],
	"stones": [],
	"spikes": []
},
{ #5-1
	"player_pos": 930,
	"balls": [
		{
			"size": "big",
			"pos": Vector2(300, 200),
			"move_right":true,
			"color": "green"
		},
		{
			"size": "full",
			"pos": Vector2(930, 200),
			"move_right":true,
			"color": "green"
		},
		{
			"size": "medium",
			"pos": Vector2(1600, 200),
			"move_right":false,
			"color": "green"
		}
	],
	"powerups": [null,null,null,Globals.POWERUP_DYNAMITE,null,null,Globals.POWERUP_SHIELD,Globals.POWERUP_TIME,Globals.POWERUP_TIME,Globals.POWERUP_COIN,Globals.POWERUP_SHIELD,Globals.POWERUP_SHIELD,Globals.POWERUP_TIME,Globals.POWERUP_WEAPON_DOUBLE,Globals.POWERUP_DYNAMITE,null,null,Globals.POWERUP_WEAPON_DOUBLE,Globals.POWERUP_DYNAMITE,Globals.POWERUP_WEAPON_DOUBLE,null,null,null,Globals.POWERUP_WEAPON_HOOK,null,],
	"stones": [],
	"spikes": []
},
{ #5-2
	"player_pos": 930,
	"balls": [
		{
			"size": "big",
			"pos": Vector2(300, 200),
			"move_right":true,
			"color": "yellow"
		},
		{
			"size": "full",
			"pos": Vector2(930, 200),
			"move_right":true,
			"color": "yellow"
		},
		{
			"size": "medium",
			"pos": Vector2(1600, 200),
			"move_right":false,
			"color": "yellow"
		}
	],
	"powerups": [null,null,Globals.POWERUP_WEAPON_PISTOL,Globals.POWERUP_DYNAMITE,Globals.POWERUP_COIN,Globals.POWERUP_WEAPON_DOUBLE,null,null,null,null,Globals.POWERUP_TIME,Globals.POWERUP_TIME,Globals.POWERUP_WEAPON_HOOK,Globals.POWERUP_DYNAMITE,null,null,null,null,Globals.POWERUP_TIME,null,Globals.POWERUP_WEAPON_PISTOL,null,null,Globals.POWERUP_WEAPON_PISTOL,Globals.POWERUP_TIME,],
	"stones": [],
	"spikes": []
},
{ #5-3
	"player_pos": 930,
	"balls": [
		{
			"size": "small",
			"pos": Vector2(100, 200),
			"move_right":false,
			"color": "yellow"
		},
		{
			"size": "medium",
			"pos": Vector2(400, 200),
			"move_right":false,
			"color": "yellow"
		},
		{
			"size": "big",
			"pos": Vector2(930, 200),
			"move_right":true,
			"color": "yellow"
		},
		{
			"size": "medium",
			"pos": Vector2(1200, 200),
			"move_right":true,
			"color": "yellow"
		},
		{
			"size": "small",
			"pos": Vector2(1600, 200),
			"move_right":true,
			"color": "yellow"
		}
	],
	"powerups": [null,null,Globals.POWERUP_TIME,null,null,null,Globals.POWERUP_COIN,null,Globals.POWERUP_SHIELD,null,null,null,null,null,null,null,null,Globals.POWERUP_DYNAMITE,Globals.POWERUP_TIME,Globals.POWERUP_TIME,Globals.POWERUP_WEAPON_DOUBLE,Globals.POWERUP_TIME,null,null,null,],
	"stones": [],
	"spikes": []
},
{ #5-4
	"player_pos": 930,
	"balls": [
		{
			"size": "big",
			"pos": Vector2(300, 200),
			"move_right":true,
			"color": "yellow"
		},
		{
			"size": "full",
			"pos": Vector2(930, 200),
			"move_right":true,
			"color": "yellow"
		},
		{
			"size": "medium",
			"pos": Vector2(1600, 200),
			"move_right":false,
			"color": "yellow"
		}
	],
	"powerups": [Globals.POWERUP_WEAPON_PISTOL,Globals.POWERUP_WEAPON_PISTOL,null,Globals.POWERUP_WEAPON_HOOK,Globals.POWERUP_WEAPON_PISTOL,null,null,null,Globals.POWERUP_COIN,Globals.POWERUP_DYNAMITE,Globals.POWERUP_TIME,null,Globals.POWERUP_TIME,Globals.POWERUP_TIME,null,Globals.POWERUP_WEAPON_HOOK,Globals.POWERUP_WEAPON_PISTOL,Globals.POWERUP_SHIELD,null,null,Globals.POWERUP_WEAPON_DOUBLE,null,Globals.POWERUP_WEAPON_PISTOL,null,null,],
	"stones": [1450],
	"spikes": []
},
{ #5-5
	"player_pos": 930,
	"balls": [
		{
			"size": "small",
			"pos": Vector2(100, 200),
			"move_right":false,
			"color": "blue"
		},
		{
			"size": "medium",
			"pos": Vector2(400, 200),
			"move_right":false,
			"color": "blue"
		},
		{
			"size": "big",
			"pos": Vector2(930, 200),
			"move_right":true,
			"color": "blue"
		},
		{
			"size": "medium",
			"pos": Vector2(1200, 200),
			"move_right":true,
			"color": "blue"
		},
		{
			"size": "small",
			"pos": Vector2(1600, 200),
			"move_right":true,
			"color": "blue"
		}
	],
	"powerups": [null,Globals.POWERUP_TIME,null,null,null,Globals.POWERUP_WEAPON_HOOK,null,null,Globals.POWERUP_COIN,null,Globals.POWERUP_DYNAMITE,null,Globals.POWERUP_SHIELD,Globals.POWERUP_WEAPON_HOOK,Globals.POWERUP_WEAPON_DOUBLE,Globals.POWERUP_TIME,null,null,Globals.POWERUP_WEAPON_PISTOL,null,Globals.POWERUP_DYNAMITE,null,Globals.POWERUP_WEAPON_HOOK,null,Globals.POWERUP_WEAPON_HOOK,],
	"stones": [],
	"spikes": []
},
{ #5-6
	"player_pos": 930,
	"balls": [
		{
			"size": "full",
			"pos": Vector2(300, 200),
			"move_right":true,
			"color": "blue"
		},
		{
			"size": "full",
			"pos": Vector2(1600, 200),
			"move_right":false,
			"color": "blue"
		}
	],
	"powerups": [null,Globals.POWERUP_SHIELD,null,null,null,Globals.POWERUP_WEAPON_DOUBLE,null,Globals.POWERUP_WEAPON_PISTOL,Globals.POWERUP_COIN,Globals.POWERUP_WEAPON_PISTOL,Globals.POWERUP_TIME,null,Globals.POWERUP_DYNAMITE,null,null,Globals.POWERUP_DYNAMITE,Globals.POWERUP_SHIELD,null,null,Globals.POWERUP_TIME,null,Globals.POWERUP_SHIELD,Globals.POWERUP_SHIELD,null,Globals.POWERUP_SHIELD,],
	"stones": [],
	"spikes": []
},
{ #5-7
	"player_pos": 930,
	"balls": [
		{
			"size": "full",
			"pos": Vector2(300, 200),
			"move_right":true,
			"color": "red"
		},
		{
			"size": "full",
			"pos": Vector2(1600, 200),
			"move_right":false,
			"color": "red"
		}
	],
	"powerups": [Globals.POWERUP_COIN,null,null,Globals.POWERUP_DYNAMITE,null,Globals.POWERUP_TIME,Globals.POWERUP_WEAPON_PISTOL,Globals.POWERUP_WEAPON_DOUBLE,null,Globals.POWERUP_WEAPON_HOOK,null,Globals.POWERUP_WEAPON_PISTOL,null,null,null,null,Globals.POWERUP_SHIELD,null,Globals.POWERUP_SHIELD,null,null,null,null,null,null,],
	"stones": [],
	"spikes": []
},
{ #5-8
	"player_pos": 930,
	"balls": [
		{
			"size": "full",
			"pos": Vector2(300, 200),
			"move_right":true,
			"color": "red"
		},
		{
			"size": "full",
			"pos": Vector2(1600, 200),
			"move_right":false,
			"color": "red"
		}
	],
	"powerups": [Globals.POWERUP_COIN,Globals.POWERUP_TIME,null,Globals.POWERUP_WEAPON_DOUBLE,null,null,null,null,Globals.POWERUP_TIME,Globals.POWERUP_WEAPON_HOOK,null,null,null,Globals.POWERUP_WEAPON_HOOK,Globals.POWERUP_WEAPON_DOUBLE,null,null,null,null,Globals.POWERUP_WEAPON_DOUBLE,null,null,null,Globals.POWERUP_SHIELD,null,],
	"stones": [],
	"spikes": [1450]
}

]


func generate_levels():
	randomize()
	for stage in range(3):
		for lvl in range (9):
			random_level(stage+3, lvl)


func random_level(stage, level):
	print("{ #"+str(stage)+"-"+str(level))
	print("	\"player_pos\": 930,")
	balls()
	powerups(25)
	stones()
	print("},")

func balls():	
	var r = randi()%4
	if r==0:
		balls0()
	if r==1:
		balls1()
	if r==2:
		balls2()
	if r==3:
		balls3()
	if r==4:
		balls4()
	if r==5:
		balls5()
		

func balls0():
	var colors = ["red", "blue", "yellow", "green"]
	var color = colors[randi() % colors.size()]
	
	print("	\"balls\": [")
	print("		{")
	print("			\"size\": \"full\",")
	print("			\"pos\": Vector2(300, 200),")
	print("			\"move_right\":true,")
	print("			\"color\": \""+color+"\"")
	print("		},")
	print("		{")
	print("			\"size\": \"full\",")
	print("			\"pos\": Vector2(1600, 200),")
	print("			\"move_right\":false,")
	print("			\"color\": \""+color+"\"")
	print("		}")
	print("	],")
	
func balls1():
	var colors = ["red", "blue", "yellow", "green"]
	var color = colors[randi() % colors.size()]
	
	print("	\"balls\": [")
	print("		{")
	print("			\"size\": \"big\",")
	print("			\"pos\": Vector2(300, 200),")
	print("			\"move_right\":true,")
	print("			\"color\": \""+color+"\"")
	print("		},")
	print("		{")
	print("			\"size\": \"full\",")
	print("			\"pos\": Vector2(930, 200),")
	print("			\"move_right\":true,")
	print("			\"color\": \""+color+"\"")
	print("		},")
	print("		{")
	print("			\"size\": \"medium\",")
	print("			\"pos\": Vector2(1600, 200),")
	print("			\"move_right\":false,")
	print("			\"color\": \""+color+"\"")
	print("		}")
	print("	],")
	
func balls2():
	var colors = ["red", "blue", "yellow", "green"]
	var color = colors[randi() % colors.size()]
	
	print("	\"balls\": [")
	print("		{")
	print("			\"size\": \"medium\",")
	print("			\"pos\": Vector2(300, 200),")
	print("			\"move_right\":false,")
	print("			\"color\": \""+color+"\"")
	print("		},")
	print("		{")
	print("			\"size\": \"full\",")
	print("			\"pos\": Vector2(930, 200),")
	print("			\"move_right\":true,")
	print("			\"color\": \""+color+"\"")
	print("		},")
	print("		{")
	print("			\"size\": \"big\",")
	print("			\"pos\": Vector2(1600, 200),")
	print("			\"move_right\":false,")
	print("			\"color\": \""+color+"\"")
	print("		}")
	print("	],")
	
func balls3():
	var colors = ["red", "blue", "yellow", "green"]
	var color = colors[randi() % colors.size()]
	
	print("	\"balls\": [")
	print("		{")
	print("			\"size\": \"small\",")
	print("			\"pos\": Vector2(100, 200),")
	print("			\"move_right\":false,")
	print("			\"color\": \""+color+"\"")
	print("		},")
	print("		{")
	print("			\"size\": \"medium\",")
	print("			\"pos\": Vector2(400, 200),")
	print("			\"move_right\":false,")
	print("			\"color\": \""+color+"\"")
	print("		},")	
	print("		{")
	print("			\"size\": \"big\",")
	print("			\"pos\": Vector2(930, 200),")
	print("			\"move_right\":true,")
	print("			\"color\": \""+color+"\"")
	print("		},")
	print("		{")
	print("			\"size\": \"medium\",")
	print("			\"pos\": Vector2(1200, 200),")
	print("			\"move_right\":true,")
	print("			\"color\": \""+color+"\"")
	print("		},")
	print("		{")
	print("			\"size\": \"small\",")
	print("			\"pos\": Vector2(1600, 200),")
	print("			\"move_right\":true,")
	print("			\"color\": \""+color+"\"")
	print("		}")
	print("	],")

func balls4():
	var colors = ["red", "blue", "yellow", "green"]
	var color = colors[randi() % colors.size()]
	
	print("	\"balls\": [")
	print("		{")
	print("			\"size\": \"small\",")
	print("			\"pos\": Vector2(100, 200),")
	print("			\"move_right\":false,")
	print("			\"color\": \""+color+"\"")
	print("		},")
	print("		{")
	print("			\"size\": \"small\",")
	print("			\"pos\": Vector2(400, 200),")
	print("			\"move_right\":false,")
	print("			\"color\": \""+color+"\"")
	print("		},")	
	print("		{")
	print("			\"size\": \"full\",")
	print("			\"pos\": Vector2(930, 200),")
	print("			\"move_right\":true,")
	print("			\"color\": \""+color+"\"")
	print("		},")
	print("		{")
	print("			\"size\": \"small\",")
	print("			\"pos\": Vector2(1200, 200),")
	print("			\"move_right\":true,")
	print("			\"color\": \""+color+"\"")
	print("		},")
	print("		{")
	print("			\"size\": \"small\",")
	print("			\"pos\": Vector2(1600, 200),")
	print("			\"move_right\":true,")
	print("			\"color\": \""+color+"\"")
	print("		}")
	print("	],")
	
func balls5():
	var colors = ["red", "blue", "yellow", "green"]
	var color = colors[randi() % colors.size()]
	
	print("	\"balls\": [")
	print("		{")
	print("			\"size\": \"full\",")
	print("			\"pos\": Vector2(300, 200),")
	print("			\"move_right\":true,")
	print("			\"color\": \""+color+"\"")
	print("		},")
	print("		{")
	print("			\"size\": \"medium\",")
	print("			\"pos\": Vector2(930, 200),")
	print("			\"move_right\":true,")
	print("			\"color\": \""+color+"\"")
	print("		},")
	print("		{")
	print("			\"size\": \"full\",")
	print("			\"pos\": Vector2(1600, 200),")
	print("			\"move_right\":true,")
	print("			\"color\": \""+color+"\"")
	print("		}")
	print("	],")

func powerups(num):	
	var powerups = ["Globals.POWERUP_WEAPON_DOUBLE", "Globals.POWERUP_WEAPON_HOOK", "Globals.POWERUP_WEAPON_PISTOL", "Globals.POWERUP_DYNAMITE", "Globals.POWERUP_SHIELD", "Globals.POWERUP_TIME"]
	var pws = []
	for i in range(num):
		if (randi()%10 < 4):
			pws.append(powerups[randi() % powerups.size()])
		else:
			pws.append("null")
			
	pws[randi()%10] = "Globals.POWERUP_COIN"
	var pws_txt = "\"powerups\": ["
	for p in pws:
		pws_txt += str(p) + ","
	pws_txt += "],"
	print("	"+pws_txt)
	
func stones():	
	var r = randi()%60
	if r < 3:
		print("	\"stones\": [350],")
		print("	\"spikes\": []")
	elif r< 6:
		print("	\"stones\": [1450],")
		print("	\"spikes\": []")
	elif r<9:
		print("	\"stones\": [350, 1450],")
		print("	\"spikes\": []")
	elif r<12:
		print("	\"stones\": [],")
		print("	\"spikes\": [350]")
	elif r<15:
		print("	\"stones\": [],")
		print("	\"spikes\": [1450]")
	elif r<18:
		print("	\"stones\": [],")
		print("	\"spikes\": [350, 1450]")
	elif r<21:
		print("	\"stones\": [350],")
		print("	\"spikes\": [1450]")
	elif r<24:
		print("	\"stones\": [1450],")
		print("	\"spikes\": [350]")
	else:
		print("	\"stones\": [],")
		print("	\"spikes\": []")
